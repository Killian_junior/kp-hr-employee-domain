/**
 * Created by Dominic on 15-Sep-2014.
 */
var _ = require('lodash');

module.exports.read = _.extend(
    require('./read_services/association-membership'),
    require('./read_services/certification'),
    require('./read_services/child'),
    require('./read_services/credential'),
    require('./read_services/education'),
    require('./read_services/emergency-contact'),
    require('./read_services/personal'),
    require('./read_services/profile'),
    require('./read_services/project'),
    require('./read_services/query'),
    require('./read_services/query-response'),
    require('./read_services/residence'),
    require('./read_services/training')

);



module.exports.write = _.extend(
    require('./write_services/association-membership'),
    require('./write_services/certifications'),
    require('./write_services/child'),
    require('./write_services/credential'),
    require('./write_services/education'),
    require('./write_services/emergency-contact'),
    require('./write_services/personal'),
    require('./write_services/profile'),
    require('./write_services/project'),
    require('./write_services/query'),
    require('./write_services/query-response'),
    require('./write_services/residence'),
    require('./write_services/training')
);