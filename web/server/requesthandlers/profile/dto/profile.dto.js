/**
 * Created by TERMINAL 7 on 10/14/2014.
 */
var ProfileObject = function(args){

    var profile = {};

    profile.category = args.category;
    profile.section = args.section;
    profile.details = args.details;
    profile.job_role = args.job_role;
    profile.appointment_date = args.appointment_date;
    profile.created = args.created;
    profile.created_by = args.created_by;
    profile.personal = args.personal;
    profile.project = args.project;
    profile.query = args.query;
    profile.training = args.training;
    profile.certifications = args.certifications;


    return profile;
};

module.exports = ProfileObject;