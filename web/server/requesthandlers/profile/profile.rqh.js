/**
 * Created by TERMINAL 7 on 10/14/2014.
 */

var hrEmployeeManager = require('domain');
var ProfileObject = require('.dto/profile.dto');
var errorHandler = require('../../config/errors');
_ = require('lodash');

exports.create = function(req, res){
    var profile = new ProfileObject(req.body);
    profile.created_by = req.user.id;

    hrEmployeeManager.write.createProfile(profile, function(err, profile){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(profile);
        }
    });
};

exports.update = function(req, res){
    var data = new ProfileObject(req.body);

    hrEmployeeManager.write.updateProfile(data, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.remove = function(req, res){
    var profileId = req.params.profileId;

    hrEmployeeManager.write.removeProfileById(profileId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.get = function(req, res){
    var profileId = req.params.profileId;
    hrEmployeeManager.read.getProfile(profileId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }   else{
            res.jsonp(result);
        }
    });
};

exports.getAll = function(req, res){
    var profiles = req.params.profiles;
    hrEmployeeManager.read.getProfiles(profiles, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else{
            res.jsonp(result);
        }
    });
};