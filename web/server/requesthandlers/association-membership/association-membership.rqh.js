/**
 * Created by TERMINAL 7 on 10/15/2014.
 */
var hrEmployeeManager = require('domain');
var AssociationMembershipObject = require('.dto/association-membership.dto');
var errorHandler = require('../../config/errors');
_ = require('lodash');

exports.create = function(req, res){
    var associationMembership = new AssociationMembershipObject(req.body);
    associationMembership.created_by = req.user.id;

    hrEmployeeManager.write.createAssociationMembership(associationMembership, function(err, obj){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(obj);
        }
    });
};

exports.update = function(req, res){
    var data = new AssociationMembershipObject(req.body);

    hrEmployeeManager.write.updateAssociationMembership(data, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.remove = function(req, res){
    var associationMembershipId = req.params.associationMembershipId;

    hrEmployeeManager.write.removeAssociationMembershipById(associationMembershipId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.get = function(req, res){
    var associationMembershipId = req.params.associationMembershipId;
    hrEmployeeManager.read.getAssociationMembership(associationMembershipId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }   else{
            res.jsonp(result);
        }
    });
};

exports.getAll = function(req, res){
    var associationMemberships = req.params.associationMemberships;
    hrEmployeeManager.read.getAssociationMemberships(associationMemberships, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else{
            res.jsonp(result);
        }
    });
};