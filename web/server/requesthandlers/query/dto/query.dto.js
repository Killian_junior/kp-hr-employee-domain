/**
 * Created by TERMINAL 7 on 10/15/2014.
 */
var QueryObject = function(args){

    var query = {};

    query.subject = args.subject;
    query.message = args.message;
    query.remarks = args.remarks;
    query.from = args.from;
    query.query_date = args.query_date;
    query.created = args.created;
    query.created_by = args.created_by;
    query.profile = args.profile;

    return query;
};

module.exports = QueryObject;