/**
 * Created by TERMINAL 7 on 10/14/2014.
 */

var hrEmployeeManager = require('domain');
var QueryObject = require('.dto/query.dto');
var errorHandler = require('../../config/errors');
_ = require('lodash');

exports.create = function(req, res){
    var query = new QueryObject(req.body);
    query.created_by = req.user.id;

    hrEmployeeManager.write.createQuery(query, function(err, query){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(query);
        }
    });
};

exports.update = function(req, res){
    var data = new QueryObject(req.body);

    hrEmployeeManager.write.updateQuery(data, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.remove = function(req, res){
    var queryId = req.params.queryId;

    hrEmployeeManager.write.removeQuery(queryId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.get = function(req, res){
    var queryId = req.params.queryId;
    hrEmployeeManager.read.getQuery(queryId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }   else{
            res.jsonp(result);
        }
    });
};

exports.getAll = function(req, res){
    var queries = req.params.queries;
    hrEmployeeManager.read.getQueries(queries, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else{
            res.jsonp(result);
        }
    });
};