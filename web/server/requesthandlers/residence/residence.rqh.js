/**
 * Created by TERMINAL 7 on 10/14/2014.
 */

var hrEmployeeManager = require('domain');
var ResidenceObject = require('.dto/residence.dto');
var errorHandler = require('../../config/errors');
_ = require('lodash');

exports.create = function(req, res){
    var residence = new ResidenceObject(req.body);
    residence.created_by = req.user.id;

    hrEmployeeManager.write.createResidence(residence, function(err, residence){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(residence);
        }
    });
};

exports.update = function(req, res){
    var data = new ResidenceObject(req.body);

    hrEmployeeManager.write.updateResidence(data, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.remove = function(req, res){
    var residenceId = req.params.residenceId;

    hrEmployeeManager.write.removeResidence(residenceId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.get = function(req, res){
    var residenceId = req.params.residenceId;
    hrEmployeeManager.read.getResidence(residenceId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }   else{
            res.jsonp(result);
        }
    });
};

exports.getAll = function(req, res){
    var residences = req.params.residences;
    hrEmployeeManager.read.getAllResidence(residences, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else{
            res.jsonp(result);
        }
    });
};