/**
 * Created by TERMINAL 7 on 10/14/2014.
 */

var hrEmployeeManager = require('domain');
var QueryResponseObject = require('.dto/query-response.dto');
var errorHandler = require('../../config/errors');
_ = require('lodash');

exports.create = function(req, res){
    var queryResponse = new QueryResponseObject(req.body);
    queryResponse.created_by = req.user.id;

    hrEmployeeManager.write.createQueryResponse(queryResponse, function(err, queryResponse){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(queryResponse);
        }
    });
};

exports.update = function(req, res){
    var data = new QueryResponseObject(req.body);

    hrEmployeeManager.write.updateQueryResponse(data, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.remove = function(req, res){
    var queryResponseId = req.params.queryResponseId;

    hrEmployeeManager.write.removeQueryResponse(queryResponseId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.get = function(req, res){
    var queryResponseId = req.params.queryResponseId;
    hrEmployeeManager.read.getQueryResponse(queryResponseId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }   else{
            res.jsonp(result);
        }
    });
};

exports.getAll = function(req, res){
    var queryResponses = req.params.queryResponses;
    hrEmployeeManager.read.getQueryResponses(queryResponses, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else{
            res.jsonp(result);
        }
    });
};