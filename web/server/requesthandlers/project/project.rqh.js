/**
 * Created by TERMINAL 7 on 10/14/2014.
 */

var hrEmployeeManager = require('domain');
var ProjectObject = require('.dto/project.dto');
var errorHandler = require('../../config/errors');
_ = require('lodash');

exports.create = function(req, res){
    var project = new ProjectObject(req.body);
    project.created_by = req.user.id;

    hrEmployeeManager.write.createProject(project, function(err, project){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(project);
        }
    });
};

exports.update = function(req, res){
    var data = new ProjectObject(req.body);

    hrEmployeeManager.write.updateProject(data, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.remove = function(req, res){
    var projectId = req.params.projectId;

    hrEmployeeManager.write.removeProjectById(projectId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.get = function(req, res){
    var projectId = req.params.projectId;
    hrEmployeeManager.read.getProject(projectId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }   else{
            res.jsonp(result);
        }
    });
};

exports.getAll = function(req, res){
    var projects = req.params.projects;
    hrEmployeeManager.read.getProjects(projects, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else{
            res.jsonp(result);
        }
    });
};