/**
 * Created by TERMINAL 7 on 10/15/2014.
 */
var ChildObject = function(args){

    var child = {};

    child.title = args.title;
    child.name = args.name;
    child.contact_nos = args.contact_nos;
    child.house_number = args.house_number;
    child.street_name = args.street_name;
    child.city_locality = args.city_locality;
    child.residence_country = args.residence_country;
    child.remarks = args.remarks;
    child.created = args.created;
    child.created_by = args.created_by;
    child.personal = args.personal;

    return child;
};

module.exports = ChildObject;