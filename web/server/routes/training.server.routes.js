/**
 * Created by TERMINAL 7 on 10/14/2014.
 */
var training = require('../requesthandlers/training');

module.exports = function(app){

    app.route('/training/create').post(training.createTraining);

    app.route('/training/update/:trainingId').put(training.updateTraining);

    app.route('/training/get/:trainingId').get(training.getTraining);

    app.route('/training/getAll').get(training.getTrainings);

    app.route('/training/delete/:trainingId').delete(training.removeTraining);
};