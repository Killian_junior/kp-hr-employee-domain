/**
 * Created by TERMINAL 7 on 10/14/2014.
 */
var profile = require('../requesthandlers/profile');

module.exports = function(app){

    app.route('/profile/create').post(profile.createProfile);

    app.route('/profile/update/:profileId').put(profile.updateProfile);

    app.route('/profile/get/:profileId').get(profile.getProfile);

    app.route('/profile/getAll').get(profile.getProfiles);

    app.route('/profile/delete/:profileId').delete(profile.removeProfileById);
};