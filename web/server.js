/**
 * Created by TERMINAL 7 on 10/14/2014.
 */
var express = require('express'),
    bodyParser = require('body-parser'),
    morgan = require('morgan'),
    mongoose = require('mongoose'),
    swig = require('swig');


var path  = require('path');



var app = express();
mongoose.connect('mongodb://localhost/hrdb');

app.engine('html', swig.renderFile);

app.set('view engine', 'html');
app.set('views', __dirname + '/server/views');

app.use(bodyParser.json());
// Enable logger (morgan)
app.use(morgan('dev'));
//if (process.env.NODE_ENV === 'development') {
//
//
//    // Disable views cache
//    app.set('view cache', false);
//} else if (process.env.NODE_ENV === 'production') {
//    app.locals.cache = 'memory';
//}

//require('./server/routes/core')(app);
require('./server/')(app);
require('./server/')(app);


app.use(express.static(path.resolve('./public')));

var port = process.env.NODE_ENV || 5050;


app.listen(port, function(){
    console.log('Starting Server at Port: '+ port)
});