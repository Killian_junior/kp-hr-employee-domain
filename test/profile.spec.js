///**
//* Created by TERMINAL 7 on 10/8/2014.
//*/
//var should = require('should');
//var mongoose = require('mongoose');
//var moment = require('moment');
//var BCServices = require('../index'),
//    User = require('./testmodels/user');
//    Category = require('./testmodels/category');
//    Section = require('./testmodels/section');
//    Personal = require('./testmodels/personal');
//
////mongoose.connect('mongodb://localhost/hrdb');
//
//describe('Profile Specifications tests', function() {
//
//    var createdProfile = {};
//
//    beforeEach(function (done) {
//        var user = new User({
//            username: 'patty'
//        });
//        var category = new Category({
//            name:'Acada'
//        });
//
//        category.save();
//
//        var section = new Section({
//            name:'Programming'
//        });
//
//        section.save();
//
//        var personal = new Personal({
//           name:'Killians test'
//        });
//
//        personal.save();
//
//
//        var profile =  {
//
//           category: category._id,
//           section: section._id,
//           details:'Back End',
//           job_role:'Unspecified',
//           appointment_date:moment('02/1/2014', 'DD/MM/YYYY'),
//           created:Date.now(),
//           created_by:user._id,
//            personal:personal._id
//
//
//
//        };
//        user.save(function(err, user){
//            if(user){
//                profile.created_by = user._id;
//                BCServices.write.createProfile(profile, function(err, result){
//                    if(err){
//                        console.log(err)
//                    }
//                    if(result){
//                        createdProfile = result;
//                    }
//                    done()
//                });
//            }
//        });
//    });
//
//    describe('Method Save', function(){
//        it('should run successfully', function(){
//            should.exist(createdProfile._id);
//        });
//    });
//
//    afterEach(function(done){
//        BCServices.write.removeProfileById(createdProfile._id, function(err, result){
//            if(!err){
//                should.exist(result);
//            }
//            User.remove().exec();
//            Category.remove().exec();
//            done();
//        });
//    });
//
//});