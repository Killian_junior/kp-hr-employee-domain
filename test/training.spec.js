/**
 * Created by TERMINAL 7 on 10/13/2014.
 */

var should = require('should');
var mongoose = require('mongoose');
var moment = require('moment');
var BCServices = require('../index'),
    User = require('./testmodels/user'),
Country = require('./testmodels/country'),
Section = require('./testmodels/section'),
Personal = require('./testmodels/personal');

mongoose.connect('mongodb://localhost/hrdb');

describe('Training Specifications tests', function() {

    var createdTraining = {};

    beforeEach(function (done) {
        var user = new User({
            username: 'patty'
        });
        var country = new Country({
            name:'Nigeria'
        });

        country.save();

        var section = new Section({
            name:'Programming'
        });

        section.save();

        var profile = new Personal({
            name:'Hooray'
        });

        profile.save();


        var training =  {
            institution: 'Brennot studios',
            from: moment('09/10/2014', 'DD/MM/YYYY'),
            to: moment('023/10/2014', 'DD/MM/YYYY'),
            course:'Biology',
            remarks: 'aye 23',
            job_role:'Unspecified',
            created: Date.now(),
            created_by: user._id,
            profile: profile._id




        };
        user.save(function(err, user){
            if(user){
                training.created_by = user._id;
                BCServices.write.createTraining(training, function(err, result){
                    if(err){
                        console.log(err)
                    }
                    if(result){
                        createdTraining = result;
                    }
                    done()
                });
            }
        });
    });

    describe('Method Save', function(){
        it('should run successfully', function(){
            should.exist(createdTraining._id);
        });
    });

    afterEach(function(done){
        BCServices.write.removeTraining(createdTraining._id, function(err, result){
            if(!err){
                should.exist(result);
            }
            User.remove().exec();
                Personal.remove().exec();
            Country.remove().exec();
            done();
        });
    });

});