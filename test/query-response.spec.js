/**
 * Created by TERMINAL 7 on 10/13/2014.
// */
//var should = require('should');
//var mongoose = require('mongoose');
//var moment = require('moment');
//var BCServices = require('../index'),
//    User = require('./testmodels/user'),
//Profile = require('./testmodels/category'),
//ProfileB = require('./testmodels/section'),
//Personal = require('../models/personal');
//
//mongoose.connect('mongodb://localhost/hrdb');
//
//describe('QueryResponse Specifications tests', function() {
//
//    var createdQueryResponse = {};
//
//    beforeEach(function (done) {
//        var user = new User({
//            username: 'patty'
//        });
//       var profile = new Profile({
//            name:'Acada'
//        });
//
//        profile.save();
//
//        var profileB = new ProfileB({
//            name:'Programming'
//        });
//
//        profileB.save();
//
//
//
//        var QueryResponse = {
//
//            subject: 'Flippant',
//            message: 'This is a QueryResponse',
//            remarks: 'first time',
//            from:profile._id,
//            query_date: Date.now(),
//            created: Date.now(),
//            created_by: user._id,
//            profile:profileB._id
//
//
//        };
//        user.save(function (err, user) {
//            if (user) {
//                QueryResponse.created_by = user._id;
//                BCServices.write.createQueryResponse(QueryResponse, function (err, result) {
//                    if (err) {
//                        console.log(err)
//                    }
//                    if (result) {
//                        createdQueryResponse = result;
//                    }
//                    done()
//                });
//            }
//        });
//    });
//
//    describe('Method Save', function () {
//        it('should run successfully', function () {
//            should.exist(createdQueryResponse._id);
//        });
//    });
//
//    afterEach(function (done) {
//        BCServices.write.removeQueryResponse(createdQueryResponse._id, function (err, result) {
//            if (!err) {
//                should.exist(result);
//            }
//            User.remove().exec();
//            Profile.remove().exec();
//            ProfileB.remove().exec();
//            done();
//        });
//    });
//
//});