/**
* Created by TERMINAL 7 on 10/6/2014.
*/
//var should = require('should');
//var mongoose = require('mongoose');
//var moment = require('moment');
//var BCServices = require('../index'),
//    User = require('./testmodels/user'),
//    Country = require('./testmodels/country'),
//    Personal = require('./testmodels/personal');
//
//mongoose.connect('mongodb://localhost/hrdb');
//
//describe('Association Membership Specifications tests', function() {
//
//    var createdAssociationMembership = {};
//
//    beforeEach(function (done) {
//        var user = new User({
//            username: 'kizzyblue'
//        });
//        var country = new Country({
//            name:'Uganda'
//        });
//
//        country.save();
//
//        var personal = new Personal({
//            name:'Uganda'
//        });
//
//        personal.save();
//
//        var AssociationMembership =  {
//
//            from:moment('09/10;2014', 'DD/MM/YYYY'),
//            to:moment('12/10;2014', 'DD/MM/YYYY'),
//            institution:' Frontline Medical Laboratory',
//            course:'MediGird',
//            remarks:'Excellent',
//            created:moment('09/10/2014', 'DD/MM/YYYY'),
//            created_by: user._id,
//            personal:personal._id
//
//
//
//        };
//        user.save(function(err, user){
//            if(user){
//                AssociationMembership.created_by = user._id;
//                BCServices.write.createAssociationMembership(AssociationMembership, function(err, result){
//                    if(err){
//                        console.log(err)
//                    }
//                    if(result){
//                        createdAssociationMembership = result;
//                    }
//                    done()
//                });
//            }
//        });
//    });
//
//    describe('Method Save', function(){
//        it('should run successfully', function(){
//            should.exist(createdAssociationMembership._id);
//        });
//    });
//
//    afterEach(function(done){
//        BCServices.write.removeAssociationMembershipById(createdAssociationMembership._id, function(err, result){
//            if(!err){
//                should.exist(result);
//            }
//            User.remove().exec();
//            Country.remove().exec();
//            Personal.remove().exec();
//            done();
//        });
//    });
//
//});