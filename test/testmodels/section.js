/**
 * Created by TERMINAL 7 on 10/7/2014.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SectionSchema = new Schema({
    name:{
        type:String,
        default:'',
        required:'Please fill out this detail',
        trim:true
    }
});

var Section = mongoose.model('Section', SectionSchema);

module.exports = Section;