/**
 * Created by TERMINAL 7 on 10/7/2014.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CategorySchema = new Schema({
    name:{
        type:String,
        default:'',
        required:'Please fill out this detail',
        trim:true
    }
});

var Category = mongoose.model('Category', CategorySchema);

module.exports = Category;