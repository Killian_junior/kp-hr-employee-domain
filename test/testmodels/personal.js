/**
 * Created by TERMINAL 7 on 10/7/2014.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PersonalSchema = new Schema({
    name:{
        type:String,
        default:'',
        required:'Please fill out this detail',
        trim:true
    }
});

var Personal = mongoose.model('PersonalTest', PersonalSchema);

module.exports = Personal;