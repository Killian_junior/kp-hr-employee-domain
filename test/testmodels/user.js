/**
 * Created by TERMINAL 7 on 10/7/2014.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var UserSchema = new Schema({
    username:{
        type:String,
        default:'',
        required:'Please fill out this detail',
        trim:true
    },
    created:{
        type:Date,
        default:Date.now
    }
});

var User = mongoose.model('User', UserSchema);

module.exports = User;