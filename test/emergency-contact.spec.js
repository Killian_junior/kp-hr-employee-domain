///**
//* Created by TERMINAL 7 on 10/9/2014.
//*/
//var should = require('should');
//var mongoose = require('mongoose');
//var moment = require('moment');
//var BCServices = require('../index'),
//    User = require('./testmodels/user'),
//   Personal = require('./testmodels/personal');
//    Country = require('./testmodels/country');
//
//mongoose.connect('mongodb://localhost/hrdb');
//
//describe('Emergency Contact Specifications tests', function() {
//
//    var createdEmergencyContact = {};
//
//    beforeEach(function (done) {
//        var user = new User({
//            username: 'kizzyblue'
//        });
//        var personal = new Personal({
//            name:'OnlyKizzy'
//        });
//
//        personal.save();
//
//        var country = new Country({
//            name: 'Uganda'
//        });
//
//        country.save();
//
//
//        var emergencyContact = {
//
//
//            title: 'Mr.',
//            name: 'Obong Ikpong',
//            contact_nos: '08147426832',
//            street_name:'Udoh stree',
//            house_number:'35',
//            city_locality:'Uyo',
//            residence_country:country._id,
//            remarks: 'aye 25',
//            created: Date.now(),
//            email:'juniorkillian@gmail.com',
//            created_by: user._id,
//            personal:personal._id
//
//
//
//
//
//        };
//        user.save(function (err, user) {
//            if (user) {
//                emergencyContact.created_by = user._id;
//                BCServices.write.createEmergencyContact(emergencyContact, function (err, result) {
//                    if (err) {
//                        console.log(err)
//                    }
//                    if (result) {
//                        createdEmergencyContact = result;
//                    }
//                    done()
//                });
//            }
//        });
//    });
//
//    describe('Method Save', function () {
//        it('should run successfully', function () {
//            should.exist(createdEmergencyContact._id);
//        });
//    });
//
//    afterEach(function (done) {
//        BCServices.write.removeEmergencyContactById(createdEmergencyContact._id, function (err, result) {
//            if (!err) {
//                should.exist(result);
//            }
//            User.remove().exec();
//            Personal.remove().exec();
//            Country.remove().exec();
//            done();
//        });
//    });
//
//});