/**
 * Created by Dominic on 02-Oct-2014.
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TrainingSchema = new Schema({
    from: {
        type: Date
    },
    to: {
        type: Date
    },
    institution: {
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the training institution'
    },
    course: {
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the training course'
    },
    remarks: {
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the training course'
    },
    job_role: {
        type: String,
        default: '',
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    created_by: {
        type: Schema.ObjectId,
        ref: 'User',
        required:'Please fill task creator'
    },
    profile:{
        type: Schema.ObjectId,
        ref: 'Profile',
        required:'Please fill this person'
    }

});

var Training = mongoose.model('Training', TrainingSchema);

module.exports = Training;