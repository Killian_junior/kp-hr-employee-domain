/**
 * Created by Dominic on 02-Oct-2014.
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CertificationSchema = new Schema({

    certification_name: {
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the certification name'
    },
    certification_type: {
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the certification name'
    },
    awarding_institution:{
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the client'
    },
    remarks: {
        type: String,
        default: '',
        trim: true
    },

    created: {
        type: Date,
        default: Date.now
    },
    created_by: {
        type: Schema.ObjectId,
        ref: 'User',
        required:'Please fill task creator'
    },
    profile:{
        type: Schema.ObjectId,
        ref: 'Profile',
        required:'Please fill this person'
    }

});

var Certification = mongoose.model('Certification', CertificationSchema);

module.exports = Certification;