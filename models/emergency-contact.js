/**
 * Created by Dominic on 02-Oct-2014.
 */


var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var EmergencyContactSchema = new Schema({

    title: {type:String, required:true},
    name: {type:String, required:true},
    contact_nos: {type:String},
    email: {type:String, required:true},

    house_number: {
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in house number'
    },
    street_name: {
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the street name'
    },
    city_locality:{
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the city or locality'
    },
    residence_country :{
        type: Schema.ObjectId,
        ref: 'Country',
        required: 'Please fill country of origin',
        trim: true
    },
    remarks: {
        type: String,
        default: '',
        trim: true
    },

    created: {
        type: Date,
        default: Date.now
    },
    created_by: {
        type: Schema.ObjectId,
        ref: 'User',
        required:'Please fill task creator'
    },
    personal:{
        type: Schema.ObjectId,
        ref: 'Personal',
        required:'Please fill this person'
    }

});

var EmergencyContactModel = mongoose.model('EmergencyContact', EmergencyContactSchema);

module.exports = EmergencyContactModel;