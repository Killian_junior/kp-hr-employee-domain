/**
 * Created by Dominic on 02-Oct-2014.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    QueryResponseSchema = require('./query-response');

var QuerySchema = new Schema({

    subject: {
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the project name'
    },
    message:{
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the client'
    },
    remarks: {
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the training course'
    },
    from:{
        type: Schema.ObjectId,
        ref: 'Profile',
        required:'Please fill this person'
    },
    query_date: {
        type: Date,
        default: Date.now
    },
    created: {
        type: Date,
        default: Date.now
    },
    created_by: {
        type: Schema.ObjectId,
        ref: 'User',
        required:'Please fill task creator'
    },
    profile:{
        type: Schema.ObjectId,
        ref: 'Profile',
        required:'Please fill this person'
    },

    responses : [QueryResponseSchema]

});

var Query = mongoose.model('Query', QuerySchema);

module.exports = Query;