/**
 * Created by Dominic on 02-Oct-2014.
 */


'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ResidenceSchema = require('./residence'),
    EmergencyContactSchema = require('./emergency-contact'),
    CredentialSchema = require('./credential'),
    EducationSchema = require('./education'),
    ChildSchema = require('./child'),
AssociationMembershipSchema = require('./association-membership');


/**
 * Personal Schema
 */
var PersonalSchema = new Schema({
    title: {
        type: String,
        default: '',
   //     required: 'Please fill title',
        trim: true
    },
    first_name: {
        type: String,
        default: '',
      //  required: 'Please fill first name',
        trim: true
    },
    surname: {
        type: String,
        default: '',
     //   required: 'Please fill surname',
        trim: true
    },
    other_names: {
        type: String,
        default: '',
        trim: true
    },
    gender: {
        type: String,
        default: '',
        enum:['Male', 'Female'],
       required: 'Please fill gender'
    },

    marital_status: {
        type: String,
        default: '',
        enum:['Single', 'Married', 'Divorced', 'Widow', 'Others'],
        required: 'Please fill gender',
        trim: true
    },
    marital_status_other:{
        type: String,
        trim: true
    },
    birth_date :{
        type: Date
       // required: 'Please fill date of birth'
    },
    country_origin :{
        type: Schema.ObjectId,
        ref: 'Country' ,
      //  required: 'Please fill country of origin',
        trim: true
    },
    core_origin_locality :{
        type: String,
     //   required: 'Please fill country of origin',
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    created_by: {
        type: Schema.ObjectId,
        ref: 'User'
       //required:'Please fill task creator'
    },
  
   kin:{
       name: {type:String},
       address: {type:String},
       contact_nos: {type:String},
       relationship:  {type:String}
   },

    spouse:{
        name: {type:String},
        address: {type:String},
        contact_nos: {type:String}
    },
    residence : [ResidenceSchema],
    emergency_contact : [EmergencyContactSchema],
    education : [EducationSchema],
    credential : [CredentialSchema],
    children: [ChildSchema],
    memberships: [AssociationMembershipSchema]


});

var Personal = mongoose.model('Personal', PersonalSchema);

module.exports = Personal;