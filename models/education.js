/**
 * Created by Dominic on 02-Oct-2014.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var EducationSchema = new Schema({
    from: {
        type: Date,
        trim: true,
        required : 'Please fill in the start date'
    },
    to: {
        type: Date,
        trim: true,
        required : 'Please fill in the end date'
    },
    institution:{
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the name of the institution'
    },
    remarks: {
        type: String,
        default: '',
        trim: true
    },

    created: {
        type: Date,
        default: Date.now
    },
    created_by: {
        type: Schema.ObjectId,
        ref: 'User',
        required:'Please fill task creator'
    },
    profile:{
        type: Schema.ObjectId,
        ref: 'Profile',
        required:'Please fill this person'
    }

});

var Education = mongoose.model('Education', EducationSchema);

module.exports = Education;