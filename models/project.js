/**
 * Created by Dominic on 02-Oct-2014.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ProjectSchema = new Schema({

    project_name: {
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the project name'
    },
    client:{
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the client'
    },
    remarks: {
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the training course'
    },
    project_role: {
        type: String,
        default: '',
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    created_by: {
        type: Schema.ObjectId,
        ref: 'User',
        required:'Please fill task creator'
    },
    profile:{
        type: Schema.ObjectId,
        ref: 'Profile',
        required:'Please fill this person'
    }

});

var Project = mongoose.model('Project', ProjectSchema);

module.exports = Project;