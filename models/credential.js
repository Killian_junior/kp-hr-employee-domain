/**
 * Created by Dominic on 02-Oct-2014.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CredentialSchema = new Schema({

    credential_name: {
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the credential name'
    },
    credential_type: {
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the credential name'
    },
    awarding_institution:{
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the awarding institution'
    },
    remarks: {
        type: String,
        default: '',
        trim: true
    },
    issue_date: {
        type: Date,
        default: Date.now
    },

    created: {
        type: Date,
        default: Date.now
    },
    created_by: {
        type: Schema.ObjectId,
        ref: 'User',
        required:'Please fill task creator'
    },
    profile:{
        type: Schema.ObjectId,
        ref: 'Profile',
        required:'Please fill this person'
    }

});

var Credential = mongoose.model('Credential', CredentialSchema);

module.exports = Credential;