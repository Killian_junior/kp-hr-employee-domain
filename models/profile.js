/**
 * Created by Dominic on 02-Oct-2014.
 */
/**
 * Created by Dominic on 02-Oct-2014.
 */


'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ProjectSchema = require('./project'),
    TrainingSchema = require('./training'),
    QuerySchema = require('./query'),
CertificationSchema = require('./certification');

/**
 * Profile Schema
 */
var ProfileSchema = new Schema({
    category:{
        type: Schema.ObjectId,ref: 'StaffCategory',required:'Please fill this person'
    },
    section:{
        type: Schema.ObjectId,ref: 'Section',required:'Please fill this person'
    },

    details: {
        type: String,default: '', trim: true
    },
    job_role: {
        type: String,default: '', trim: true
    },

    appointment_date :{
        type: Date, required: 'Please fill date of employment'
    },

    created: {
        type: Date, default: Date.now
    },
    created_by: {
        type: Schema.ObjectId,  ref: 'User', required:'Please fill task creator'
    },
    personal:{
        type: Schema.ObjectId,  ref: 'Personal',
         required:'Please fill this person'
    },

    project : [ProjectSchema],
    query : [QuerySchema],
    training : [TrainingSchema],
    certifications: [CertificationSchema]

});

var Profile = mongoose.model('Profile', ProfileSchema);

module.exports = Profile;