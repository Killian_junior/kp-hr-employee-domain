/**
 * Created by Dominic on 02-Oct-2014.
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var AssociationMembershipSchema = new Schema({
    from: {
        type: Date
    },
    to: {
        type: Date
    },
    institution: {
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the training institution'
    },
    course: {
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the training course'
    },
    remarks: {
        type: String,
        default: '',
        trim: true,
        required : 'Please fill in the training course'
    },
    membership_type: {
        type: String,
        default: '',
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    created_by: {
        type: Schema.ObjectId,
        ref: 'User',
        required:'Please fill task creator'
    },
    personal:{
        type: Schema.ObjectId,
        ref: 'Personal',
        required:'Please fill this person'
    }

});

var AssociationMembership = mongoose.model('AssociationMembership', AssociationMembershipSchema);

module.exports = AssociationMembership;