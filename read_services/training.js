/**
 * Created by TERMINAL 7 on 10/3/2014.
 */

var Training = require('../models/training');

exports.getTraining = function(training, next) {

    Training.findOne({_id:training._id}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

exports.getTrainings = function(args, next) {

    Training.find({}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};
