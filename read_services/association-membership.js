/**
 * Created by TERMINAL 7 on 10/3/2014.
 */

var associationMembership = require('../models/association-membership');

exports.getAssociationMembership = function(associationMembershipId, next) {

    associationMembership.findById({_id:associationMembershipId._id}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

exports.getAssociationMemberships = function(args, next) {

    associationMembership.find({},
        function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

