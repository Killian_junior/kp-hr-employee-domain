/**
 * Created by TERMINAL 7 on 10/3/2014.
 */

var Certification = require('../models/certification');

exports.getCertification = function(certification, next) {

    Certification.findOne({_id:certification._id}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

exports.getCertifications = function(args, next) {

    Certification.find({}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

