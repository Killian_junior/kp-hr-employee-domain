/**
 * Created by TERMINAL 7 on 10/3/2014.
 */

var EmergencyContact = require('../models/emergency-contact');

exports.getEmergencyContact = function(emergencyContact, next) {

    EmergencyContact.findOne({_id:emergencyContact._id}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

exports.getEmergencyContacts = function(args, next) {

    EmergencyContact.find({}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

