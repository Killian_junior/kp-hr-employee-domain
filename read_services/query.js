/**
 * Created by TERMINAL 7 on 10/3/2014.
 */

var Query = require('../models/query');

exports.getQuery = function(query, next) {

    Query.findOne({_id:query._id}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

exports.getQueries = function(args, next) {

    Query.find({}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

