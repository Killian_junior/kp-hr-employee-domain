/**
 * Created by TERMINAL 7 on 10/3/2014.
 */

var Child = require('../models/child');

exports.getChild = function(child, next) {

    Child.findOne({_id:child._id},
        function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

exports.getChildren = function(args, next) {

    Child.find({},
        function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

