/**
 * Created by TERMINAL 7 on 10/3/2014.
 */

var Project = require('../models/project');

exports.getProject = function(project, next) {

    Project.findOne({_id:project._id}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

exports.getProjects = function(args, next) {

    Project.find({}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

