/**
 * Created by TERMINAL 7 on 10/3/2014.
 */

var Education = require('../models/education');

exports.getEducation = function(education, next) {

    Education.findOne({_id:education._id},
        function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

exports.getEducations = function(args, next) {

    Education.find({}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

