/**
 * Created by TERMINAL 7 on 10/3/2014.
 */

var Residence = require('../models/residence');

exports.getResidence = function(residence, next) {

    Residence.findOne({_id:residence._id}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

exports.getAllResidence = function(args, next) {

    Residence.find({}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};



