/**
 * Created by TERMINAL 7 on 10/3/2014.
 */

var Profile = require('../models/profile');


exports.getProfile = function(profile, next) {

    Profile.findOne({_id:profile._id}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

exports.getProfiles = function(args, next) {

    Profile.find({}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

