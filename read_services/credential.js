/**
 * Created by TERMINAL 7 on 10/3/2014.
 */

var Credential = require('../models/credential');

exports.getCredential = function(credential, next) {

    Credential.findOne({_id:credential._id},
        function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

exports.getCredentials = function(args, next) {

    Credential.find({},
        function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

