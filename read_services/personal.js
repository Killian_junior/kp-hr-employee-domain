/**
 * Created by TERMINAL 7 on 10/3/2014.
 */

var Personal = require('../models/personal');

exports.getPersonal = function(personal, next) {

    Personal.findOne({_id:personal._id}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

exports.getPersonals = function(args, next) {

    Personal.find({}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

