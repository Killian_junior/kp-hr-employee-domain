/**
 * Created by TERMINAL 7 on 10/3/2014.
 */

var QueryResponse = require('../models/query-response');

exports.getQueryResponse = function(queryResponse, next) {

    QueryResponse.findOne({_id:queryResponse._id}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

exports.getQueryResponses = function(args, next) {

    QueryResponse.find({}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

