/**
 * Created by TERMINAL 7 on 10/3/2014.
 */
var Residence = require('../models/residence');
//var ResidenceObject = require('../dto/

_ = require('lodash');

exports.createResidence = function(args, next) {
    var residence = Residence(args);

    residence.save(function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){

            console.log(result);
            return  next(null, result);

        }
    });

};

exports.updateResidence = function(args, next) {
    Residence.findById(args._id,
        function(err, doc){
            if(err){
                return  next(err, null);
            }
            if(doc){
                doc = _.extend(doc, args);

                doc.save(function(err){
                    if(!err){
                        return  next(null, true);
                    }
                });

            }
        });

};

exports.removeResidence = function(residenceId, next) {

    Residence.findById(residenceId,
        function (err, doc) {
            if (err) {
                return  next(err, null);
            }
            if (doc) {
                doc.remove(function (err) {
                    if (err) {
                        return next(err, null);
                    } else {
                        return next(null, doc);
                    }
                })

            }
        });

};