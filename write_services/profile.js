/**
 * Created by TERMINAL 7 on 10/3/2014.
 */
var Profile = require('../models/profile');
//var ProfileObject = require('../dto/profile.dto');
        _ = require('lodash');

exports.createProfile = function(args, next) {
    var profile = new Profile(args);

    profile.save(function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            console.log(result);
            return next(null, result)

        }
    });

};

exports.updateProfile = function(args, next) {

    Profile.findById(args._id,
        function(err, doc){
        if(err){
            return  next(err, null);
        }
        if(doc){
           doc = _.extend(doc, args);

            doc.save(function(err){
                if(!err){
                    return  next(null, true);
                }
            });

        }
    });

};

exports.removeProfileById = function(profileId, next) {

    Profile.findById(profileId,
        function (err, doc) {
            if (err) {
                return  next(err, null);
            }
            if (doc) {
                doc.remove(function (err) {
                    if (err) {
                        return next(err, null);
                    } else {
                        return next(null, doc);
                    }
                })

            }
        });

};