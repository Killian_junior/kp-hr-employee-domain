/**
 * Created by TERMINAL 7 on 10/3/2014.
 */
var Child = require('../models/child');
//var ChildObject = require('../dto/child.dto');
_ = require('lodash');

exports.createChild = function(args, next) {
    var child = new Child(args);
    child.save( function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){

            console.log(result);
            return  next(null, result);

        }
    });

};

exports.updateChild = function(args, next) {
    Child.findById(args._id,
        function(err, doc){
            if(err){
                return  next(err, null);
            }
            if(doc){
                doc = _.extend(doc, args);

                doc.save(function(err){
                    if(!err){
                        return  next(null, true);
                    }
                });

            }
        });


};

exports.removeChildById = function(childId, next) {

    Child.findById(childId,
        function (err, doc) {
            if (err) {
                return  next(err, null);
            }
            if (doc) {
                doc.remove(function (err) {
                    if (err) {
                        return next(err, null);
                    } else {
                        return next(null, doc);
                    }
                })

            }
        });

};