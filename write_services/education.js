/**
 * Created by TERMINAL 7 on 10/3/2014.
 */
var Education = require('../models/education');
//var EducationObject = require('../dto/education.dto');
     _ = require('lodash');

exports.createEducation = function(args, next) {
    var education = new Education(args);

    education.save(function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){

            console.log(result);
            return  next(null, result);

        }
    });

};

exports.updateEducation = function(args, next) {

    Education.findById(args._id,
        function(err, doc){
            if(err){
                return  next(err, null);
            }
            if(doc){
                doc = _.extend(doc, args);

                doc.save(function(err){
                    if(!err){
                        return  next(null, true);
                    }
                });

            }
        });
};

exports.removeEducationById = function(educationId, next) {

    Education.findById(educationId,
        function (err, doc) {
            if (err) {
                return  next(err, null);
            }
            if (doc) {
                doc.remove(function (err) {
                    if (err) {
                        return next(err, null);
                    } else {
                        return next(null, doc);
                    }
                })

            }
        });

};