/**
 * Created by TERMINAL 7 on 10/3/2014.
 */
var Training = require('../models/training');
//var TrainingObject = require('../dto/training.dto');
_ = require('lodash');

exports.createTraining = function(args, next) {
    var training = new Training(args);

    training.save(function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){

            console.log(result);
            return  next(null, result);

        }
    });

};

exports.updateTraining = function(args, next) {
    Training.findById(args._id,
        function(err, doc){
            if(err){
                return  next(err, null);
            }
            if(doc){
                doc = _.extend(doc, args);

                doc.save(function(err){
                    if(!err){
                        return  next(null, true);
                    }
                });

            }
        });


};

exports.removeTraining = function(trainingId, next) {

    Training.findById(trainingId,
        function (err, doc) {
            if (err) {
                return  next(err, null);
            }
            if (doc) {
                doc.remove(function (err) {
                    if (err) {
                        return next(err, null);
                    } else {
                        return next(null, doc);
                    }
                })

            }
        });

};