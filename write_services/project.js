/**
 * Created by TERMINAL 7 on 10/3/2014.
 */
var Project = require('../models/project');
//var ProjectObject = require('../dto/project.dto');

_ = require('lodash');


exports.createProject = function(args, next) {
    var project = new Project(args);

    project.save(function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){

            console.log(result);
            return  next(null, result);

        }
    });

};

exports.updateProject = function(args, next) {

    Project.findById(args._id,
        function(err, doc){
            if(err){
                return  next(err, null);
            }
            if(doc){
                doc = _.extend(doc, args);

                doc.save(function(err){
                    if(!err){
                        return  next(null, true);
                    }
                });

            }
        });


};

exports.removeProjectById = function(profileId, next) {

    Project.findById(profileId,
        function (err, doc) {
            if (err) {
                return  next(err, null);
            }
            if (doc) {
                doc.remove(function (err) {
                    if (err) {
                        return next(err, null);
                    } else {
                        return next(null, doc);
                    }
                })

            }
        });

};