/**
 * Created by TERMINAL 7 on 10/3/2014.
 */
var Query = require('../models/query');
//var QueryObject = require('../dto/query.dto');
_ = require('lodash');


exports.createQuery = function(args, next) {
    var query = new Query(args);

    query.save(function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            console.log(result);
            return next(null, result)

        }
    });



};

exports.updateQuery = function(args, next) {

    Query.findById(args._id,
        function(err, doc){
            if(err){
                return  next(err, null);
            }
            if(doc){
                doc = _.extend(doc, args);

                doc.save(function(err){
                    if(!err){
                        return  next(null, true);
                    }
                });

            }
        });

};

exports.removeQuery = function(queryId, next) {

    Query.findById(queryId,
        function (err, doc) {
            if (err) {
                return  next(err, null);
            }
            if (doc) {
                doc.remove(function (err) {
                    if (err) {
                        return next(err, null);
                    } else {
                        return next(null, doc);
                    }
                })

            }
        });

};