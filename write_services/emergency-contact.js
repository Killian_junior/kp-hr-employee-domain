/**
 * Created by TERMINAL 7 on 10/3/2014.
 */
var EmergencyContact = require('../models/emergency-contact');
//var EmergencyContactObject = require('../dto/emergency-contact.dto');
          _ = require('lodash');

exports.createEmergencyContact = function(args, next) {
    var emergency = new EmergencyContact(args);

    emergency.save(function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){

            console.log(result);
            return  next(null, result);

        }
    });

};

exports.updateEmergencyContact = function(args, next) {

    EmergencyContact.findById(args._id,
        function(err, doc){
            if(err){
                return  next(err, null);
            }
            if(doc){
                doc = _.extend(doc, args);

                doc.save(function(err){
                    if(!err){
                        return  next(null, true);
                    }
                });

            }
        });
};

exports.removeEmergencyContactById = function(emergencyContactId, next) {

    EmergencyContact.findById(emergencyContactId,
        function (err, doc) {
            if (err) {
                return  next(err, null);
            }
            if (doc) {
                doc.remove(function (err) {
                    if (err) {
                        return next(err, null);
                    } else {
                        return next(null, doc);
                    }
                })

            }
        });

};