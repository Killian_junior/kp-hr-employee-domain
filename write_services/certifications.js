/**
 * Created by TERMINAL 7 on 10/3/2014.
 */
var Certification = require('../models/certification');
//var CertificationObject = require('../dto/certification.dto');

_ = require('lodash');



exports.createCertification = function(args, next) {
    var certification = new Certification(args);

    certification.save(function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            console.log(result);
            return next(null, result)

        }
    });

};

exports.updateCertification = function(args, next) {

    Certification.findById(args._id,
        function(err, doc){
            if(err){
                return  next(err, null);
            }
            if(doc){
                doc = _.extend(doc, args);

                doc.save(function(err){
                    if(!err){
                        return  next(null, true);
                    }
                });

            }
        });

};

exports.removeCertificationById = function(certificationId, next) {

    Certification.findById(certificationId,
        function (err, doc) {
            if (err) {
                return  next(err, null);
            }
            if (doc) {
                doc.remove(function (err) {
                    if (err) {
                        return next(err, null);
                    } else {
                        return next(null, doc);
                    }
                })

            }
        });

};