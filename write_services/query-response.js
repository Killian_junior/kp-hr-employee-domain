/**
 * Created by TERMINAL 7 on 10/3/2014.
 */
var QueryResponse = require('../models/query-response');
//var QueryResponseObject = require('../dto/query-response.dto');

_ = require('lodash');

exports.createQueryResponse = function(args, next) {
    var queryResponse = new QueryResponse(args);

    queryResponse.save(function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){

            console.log(result);
            return  next(null, result);

        }
    });

};

exports.updateQueryResponse = function(args, next) {

    QueryResponse.findById(args._id,
        function(err, doc){
            if(err){
                return  next(err, null);
            }
            if(doc){
                doc = _.extend(doc, args);

                doc.save(function(err){
                    if(!err){
                        return  next(null, true);
                    }
                });

            }
        });


};

exports.removeQueryResponse = function(responseId, next) {

    QueryResponse.findById(responseId,
        function (err, doc) {
            if (err) {
                return  next(err, null);
            }
            if (doc) {
                doc.remove(function (err) {
                    if (err) {
                        return next(err, null);
                    } else {
                        return next(null, doc);
                    }
                })

            }
        });

};