/**
 * Created by TERMINAL 7 on 10/3/2014.
 */
var Credential = require('../models/credential');
//var CredentialObject = require('../dto/credential.dto');
_ = require('lodash');

exports.createCredential = function(args, next) {
    var credential = new Credential(args);

    credential.save(function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){

            console.log(result);
            return  next(null, result);

        }
    });

};

exports.updateCredential = function(args, next) {
    Credential.findById(args._id,
        function(err, doc){
            if(err){
                return  next(err, null);
            }
            if(doc){
                doc = _.extend(doc, args);

                doc.save(function(err){
                    if(!err){
                        return  next(null, true);
                    }
                });

            }
        });

};

exports.removeCredentialById = function(credentialId, next) {

    Credential.findById(credentialId,
        function (err, doc) {
            if (err) {
                return  next(err, null);
            }
            if (doc) {
                doc.remove(function (err) {
                    if (err) {
                        return next(err, null);
                    } else {
                        return next(null, doc);
                    }
                })

            }
        });

};