/**
 * Created by TERMINAL 7 on 10/3/2014.
 */
var associationMembership = require('../models/association-membership');
//var associationMembershipObject = require('../dto/association-membership.dto');
_ = require('lodash');

exports.createAssociationMembership = function(args, next) {
    var obj = new associationMembership(args);

    obj.save(function(err, result){
        if(err){
            return next(err, null);
        }
        if(result){
            console.log(result);
            return next(null, result);
        }
    })

};

exports.updateAssociationMembership = function(args, next) {

    associationMembership.findById(args._id,
        function(err, doc){
            if(err){
                return  next(err, null);
            }
            if(doc){
                doc = _.extend(doc, args);

                doc.save(function(err){
                    if(!err){
                        return  next(null, true);
                    }
                });

            }
        });

};

exports.removeAssociationMembershipById = function(membershipId, next) {

    associationMembership.findById(membershipId,
        function (err, doc) {
            if (err) {
                return  next(err, null);
            }
            if (doc) {
                doc.remove(function (err) {
                    if (err) {
                        return next(err, null);
                    } else {
                        return next(null, doc);
                    }
                })

            }
        });

};