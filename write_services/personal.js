/**
 * Created by TERMINAL 7 on 10/3/2014.
 */
var Personal = require('../models/personal');
//var PersonalObject = require('../dto/personal.dto');
_ = require('lodash');


exports.createPerson = function(args, next) {
    var person = new Personal(args);

    person.save(function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){

            console.log(result);
            return  next(null, result);

        }
    });

};

exports.updatePerson = function(args, next) {

    Personal.findById(args._id,
        function(err, doc){
        if(err){
            return  next(err, null);
        }
        if(doc){
            doc = _.extend(doc, args);

            doc.save(function(err){
                if(!err){
                    return  next(null, true);
                }
            })

        }
    });

};

exports.removePersonalById = function(personId, next) {

    Personal.findById(personId,
        function(err, doc){
            if(err){
                return  next(err, null);
            }
            if(doc) {
                doc.remove(function(err){
                    if(err){
                        return next(err, null);
                    }else{
                        return next(null, doc);
                    }
                })

            }
        });

};