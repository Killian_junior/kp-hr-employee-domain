/**
 * Created by TERMINAL 7 on 10/6/2014.
 */
var ResidenceObject = function(args){

    var residence = {};

    residence.house_number = args.house_number;
    residence.street_name = args.street_name;
    residence.city_locality = args.city_locality;
    residence.residence_country = args.residence_country;
    residence.created = args.created;
    residence.created_by = args.created_by;
    residence.personal = args.personal;




    if(args._id){
        residence._id = args._id;
    }
};

module.exports = ResidenceObject;