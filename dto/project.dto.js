/**
 * Created by TERMINAL 7 on 10/6/2014.
 */
var ProjectObject = function(args){

    var project = {};

    project.project_name = args.project_name;
    project.client = args.client;
    project.remarks = args.remarks;
    project.project_role = args.project_role;
    project.created = args.created;
    project.created_by = args.created_by;
    project.profile = args.profile;



    if(args._id){
        project._id = args._id;
    }
};

module.exports = ProjectObject;