/**
 * Created by TERMINAL 7 on 10/6/2014.
 */
var EducationObject = function(args){

    var education = {};

    education.from = args.from;
    education.to = args.to;
    education.institution = args.institution;
    education.remarks = args.remarks;
    education.created = args.created;
    education.created_by = args.created_by;
    education.profile = args.profile;

    if(args._id){
        education._id = args._id;
    }
};

module.exports = EducationObject;