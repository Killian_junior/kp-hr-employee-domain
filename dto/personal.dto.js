/**
 * Created by TERMINAL 7 on 10/6/2014.
 */
var PersonalObject = function(args) {

    var personal = {};

    personal.title = args.title;
    personal.first_name = args.first_name;
    personal.surname = args.surname;
    personal.other_names = args.other_names;
    personal.gender = args.gender;
    personal.marital_status = args.marital_status;
    personal.marital_status_other = args.marital_status_other;
    personal.birth_date = args.birth_date;
    personal.country_origin = args.country_origin;
    personal.core_origin_locality = args.core_origin_locality;
    personal.created = args.created;
    personal.created_by = args.created_by;
    personal.kin = args.kin;
    personal.spouse = args.spouse;
    personal.residence = args.residence;
    personal.emergency_contact = args.emergency_contact;
    personal.education = args.education;
    personal.credential = args.credential;
    personal.children = args.children;
    personal.memberships = args.memberships;

    return personal;

};

module.exports = PersonalObject;