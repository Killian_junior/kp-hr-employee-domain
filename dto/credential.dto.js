/**
 * Created by TERMINAL 7 on 10/6/2014.
 */
var CredentialObject = function(args){

    var credential = {};

    credential.credential_name = args.credential_name;
    credential.credential_type = args.credential_type;
    credential.awarding_institution = args.awarding_institution;
    credential.remarks = args.remarks;
    credential.issue_date = args.issue_date;
    credential.created = args.created;
    credential.created_by = args.created_by;
    credential.profile = args.profile;

    if(args._id){
        credential._id = args._id;
    }
};

module.exports = CredentialObject;